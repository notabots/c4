local moduleInfo = {
	name = "c4MessageSenderSynced",
	desc = "Internal API",
	author = "PepeAmpere",
	date = "2017-02-19",
	license = "MIT",
}

local newSendCustomMessage = {
	["C4FullUpdate"] = function(waypoints, playerID)
		message.SendSyncedToUnsynced({
			subject = "C4FullUpdate",
			waypoints = waypoints,
			playerID = playerID,
		})
	end,

	["C4WaypointUpdated"] = function(waypoint)
		message.SendSyncedToUnsynced({
			subject = "C4WaypointUpdated",
			waypoint = waypoint,
		})
	end,
	
	["C4WaypointRemoved"] = function(waypoint)
		message.SendSyncedToUnsynced({
			subject = "C4WaypointRemoved",
			waypoint = waypoint,
		})
	end,
	
	["C4EdgeUpdated"] = function(edge)
		message.SendSyncedToUnsynced({
			subject = "C4EdgeUpdated",
			edge = edge,
		})
	end,
}

-- END OF MODULE DEFINITIONS --

-- update global structures 
message.AttachCustomSender(newSendCustomMessage, moduleInfo)
