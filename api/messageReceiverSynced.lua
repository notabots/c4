local moduleInfo = {
	name = "c4MessageReceiverSynced",
	desc = "API for rtriggering synced code.",
	author = "PepeAmpere",
	date = "2019-01-28",
	license = "MIT",
}

-- 1) Called from widgets via message.SendRules({subject = "<func_name>", ...})
-- 2) Called from synced gadgets via message.SendSyncedRules({subject = "<func_name>", ...})

local newReceiveCustomMessage = {

	["RequestAllWaypoints"] = function(decodedMsg, playerID, context)
		c4.Init(playerID)
	end,
	
	["RequestAddEdge"] = function(decodedMsg, playerID, context)
		local fail = true
		if 
			decodedMsg ~= nil
		then
			local edge = decodedMsg.edge
			if 
				edge ~= nil and
				edge.wp1 ~= nil and
				edge.wp2 ~= nil and
				edge.edgeData ~= nil
			then
				fail = false
				c4.AddEdge(edge, playerID)
				if playerID == nil then -- temp catch
					Spring.Echo("[c4MessageReceiverSynced][RequestAddEdge] No playerID")
				end
			end
		end
		
		if fail then
			Spring.Echo(
				"[c4MessageReceiverSynced][RequestAddEdge] Invalid waypoint edge request"
			)
		end
	end,
	
	["RequestRemoveEdge"] = function(decodedMsg, playerID, context)
		local fail = true
		if 
			decodedMsg ~= nil
		then
			local edge = decodedMsg.edge
			if 
				edge ~= nil and
				edge.wp1 ~= nil and
				edge.wp2 ~= nil
			then
				fail = false
				c4.RemoveEdge(edge, playerID)
			end
		end
		
		if fail then
			Spring.Echo(
				"[c4MessageReceiverSynced][RequestRemoveEdge] Invalid waypoint edge request"
			)
		end
	end,
	
	["RequestAddWaypoint"] = function(decodedMsg, playerID, context)
		local fail = true
		if 
			decodedMsg ~= nil
		then
			local waypoint = decodedMsg.waypoint
			if 
				waypoint ~= nil
			then
				if (playerID == nil) then
					playerID = decodedMsg.waypoint:GetOwnerID() -- if sent from Gadget
				end
				
				if (playerID ~= nil) then
					fail = false
					c4.AddWaypoint(waypoint, playerID)
				end
			end
		end
		
		if fail then
			Spring.Echo(
				"[c4MessageReceiverSynced][RequestAddWaypoint] Invalid waypoint add request"
			)
		end
	end,
	
	["RequestRemoveWaypoint"] = function(decodedMsg, playerID, context)
		local fail = true
		if 
			decodedMsg ~= nil
		then
			local waypoint = decodedMsg.waypoint
			if 
				waypoint ~= nil
			then
				local ownerID = decodedMsg.waypoint:GetOwnerID()
				if (playerID == nil) then
					playerID = ownerID -- if sent from Gadget
				end
				
				if (playerID == ownerID) then
					fail = false
					c4.RemoveWaypoint(waypoint, playerID)
				end
			end
		end
		
		if fail then
			Spring.Echo(
				"[c4MessageReceiverSynced][RemoveWaypoint] Invalid waypoint add request"
			)
		end
	end,
	
	["RequestUpdateWaypoint"] = function(decodedMsg, playerID, context)
		local fail = true
		if 
			decodedMsg ~= nil
		then
			local waypoint = decodedMsg.waypoint
			if 
				waypoint ~= nil
			then
				local ownerID = decodedMsg.waypoint:GetOwnerID()
				if (playerID == nil) then
					playerID = ownerID -- if sent from Gadget
				end
				
				if (playerID == ownerID) then
					fail = false
					c4.UpdateWaypoint(waypoint, playerID)
				end
			end
		end
		
		if fail then
			Spring.Echo(
				"[c4MessageReceiverSynced][UpdateWaypoint] Invalid waypoint add request"
			)
		end
	end,
}

-- END OF MODULE DEFINITIONS --

-- update global structures 
message.AttachCustomReceiver(newReceiveCustomMessage, moduleInfo)
