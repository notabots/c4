local MODULE_NAME = "c4"
Spring.Echo("-- " .. MODULE_NAME .. " LOADING --")

------------------------------------------------------

-- MANDATORY
-- check required modules
if (modules == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory config [modules] listing paths for modules is missing") end
if (attach == nil) then Spring.Echo("[" .. MODULE_NAME .. "] ERROR: required madatory library [attach] for loading files and modules is missing") end

-- there has to exist modules table which is included before init is called
local thisModuleData = modules[MODULE_NAME]
local THIS_MODULE_DATA_PATH = thisModuleData.data.path

------------------------------------------------------

-- OPTIONAL CONFIG LOAD 
local THIS_MODULE_CONFIG_PATH = thisModuleData.config.path -- custom configs folder (optional)
local listOfFiles = thisModuleData.config.files -- list of files in configPath
attach.try.ModuleOptionalConfigs(THIS_MODULE_CONFIG_PATH, listOfFiles)

------------------------------------------------------

-- LOAD INTERNAL MODULE FUNCTIONALITY
local c4
if (widget) then
	c4 = attach.File(THIS_MODULE_DATA_PATH .. "c4.lua") -- core functions
else
	if (gadgetHandler:IsSyncedCode()) then
		c4 = attach.File(THIS_MODULE_DATA_PATH .. "c4.lua") -- core functions
		attach.File(THIS_MODULE_DATA_PATH .. "api/messageReceiverSynced.lua") -- RECEIVER of SYNCED from WIDGET
		attach.File(THIS_MODULE_DATA_PATH .. "api/messageSenderSynced.lua") -- SENDER of SYNCED to WIDGET
	else
	end
end

------------------------------------------------------

Spring.Echo("-- " .. MODULE_NAME .. " LOADING FINISHED --")

return c4

