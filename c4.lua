local moduleInfo = {
	name = "c4",
	desc = "c4 core",
	author = "PepeAmpere",
	date = "2019-01-28",
	license = "MIT",
}

c4 = {
	["Init"] = function(playerID)
		sendCustomMessage.C4FullUpdate(waypoints, playerID)
	end,
	
	["AddEdge"] = function(edge, playerID)
		local ID1 = edge.wp1:GetID()
		local ID2 = edge.wp2:GetID()
		local wp1, wp2 = waypoints[ID1], waypoints[ID2]
		
		if ID1 == -1 then
			wp1 = c4.AddWaypoint(edge.wp1, playerID)
		end
		if ID2 == -1 then
			wp2 = c4.AddWaypoint(edge.wp2, playerID)
		end
		
		wp1, wp2 = wp1:Connect(wp2, edge.edgeData)
		
		sendCustomMessage.C4EdgeUpdated({
			wp1 = wp1,
			wp2 = wp2,
		})
	end,
	
	["RemoveEdge"] = function(edge, playerID)
		local ID1 = edge.wp1:GetID()
		local ID2 = edge.wp2:GetID()
		local wp1, wp2 = waypoints[ID1], waypoints[ID2]
		wp1, wp2 = wp1:Disconnect(wp2)
		
		sendCustomMessage.C4EdgeUpdated({
			wp1 = wp1,
			wp2 = wp2,
		})
	end,
	
	["AddWaypoint"] = function(newWaypoint, playerID)
		local _,_,_,teamID,allyID = Spring.GetPlayerInfo(playerID)
		local newID = #waypoints+1
		local newLayerID = allyID * 1000 + teamID * 100 + playerID * 10
		newWaypoint = newWaypoint:SetID(newID)
		newWaypoint = newWaypoint:SetLayerID(newWaypoint:GetLayerID() + newLayerID) -- set global layer ID
		waypoints[newID] = newWaypoint
		
		sendCustomMessage.C4WaypointUpdated(newWaypoint)
		
		return newWaypoint
	end,
	
	["RemoveWaypoint"] = function(waypoint, playerID)
		local wpID = waypoint:GetID()
		local pairsToRemove = {}
		local waypointsToUpdate = {}
		waypoints, pairsToRemove = waypoint:DisconnectAll(waypoints)
		waypoints[wpID] = nil
		
		for i=1, #pairsToRemove do
			waypointsToUpdate[pairsToRemove[i][1]] = true
			waypointsToUpdate[pairsToRemove[i][2]] = true
		end
		waypointsToUpdate[wpID] = nil -- do not update removed waypoint
		for ID, _ in pairs (waypointsToUpdate) do
			sendCustomMessage.C4WaypointUpdated(waypoints[ID])
		end
		
		sendCustomMessage.C4WaypointRemoved(waypoint)
		
		return newWaypoint
	end,
	
	["UpdateWaypoint"] = function(waypoint, playerID)
		local wpID = waypoint:GetID()
		waypoints[wpID] = waypoint
		
		sendCustomMessage.C4WaypointUpdated(waypoint)
		
		return newWaypoint
	end,
	
	["FindClosestWaypoint"] = function(position)
		local closestDistance = math.huge
		local closestWaypointID = -1
		if waypoints ~= nil then
			for ID, wp in pairs(waypoints) do
				local distance = position:Distance(wp:GetPosition())
				if distance < closestDistance then
					closestDistance = distance
					closestWaypointID = ID
				end
			end
		
			return waypoints[closestWaypointID], closestDistance
		end
	end,
	
	["FindWaypoint"] = function(position, maxDistance)
		local wp, distance = c4.FindClosestWaypoint(position)
		if (distance or math.huge) < maxDistance then
			return wp
		end
		return nil
	end,
}

return c4